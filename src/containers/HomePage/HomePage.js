import React, {useEffect, useState} from 'react';
import classes from './HomePage.module.scss';
import {fetchDataRequest} from "../../strore/actions/dataActions";
import {useSelector, useDispatch} from 'react-redux';
import BoxItem from "../../components/BoxItem/BoxItem";
import CircularProgress from "../../components/CircularProgress/CircularProgress";

const HomePage = () => {
    const dispatch = useDispatch();
    const [filter, setFilter] = useState(null);
    const [isShown, setIsShown] = useState(null);
    const {data, fetchLoading, fetchError} = useSelector(state => state.data);

    let dataWithAlliance = data;
    if(filter){
        dataWithAlliance = dataWithAlliance.filter(d => d.alliance === filter);
    }

    const radioButtonChangeHandler = e => {
        const {value} = e.target;

        if(filter === value){
            setFilter(null);
        }else{
            setFilter( value);
        }
    };

    useEffect(() => {
        dispatch(fetchDataRequest());
    }, [dispatch]);

    if(fetchError) return <h4 className={classes.error}>Sorry, something went wrong</h4>

    return fetchLoading ? (
        <CircularProgress/>
        ) : (
        <main>
            <h1 className={classes.main_title}>Airlines</h1>
            <div className={classes.filter__checkbox}>
                <h4 className={classes.filter__checkbox__title}>Filter by Alliance</h4>
                <fieldset className={classes.filter__checkbox__row}>
                    <div className={classes.filter__checkbox__box}>
                        <input
                            type="radio" name="alliance" value="OW" id="OW"
                            className={classes.filter__checkbox__app_field}
                            onClick={radioButtonChangeHandler}
                        />
                        <label htmlFor="OW" className={`${classes.filter__checkbox__label} ${filter ? classes.filter__checkbox__label_symbol : ''}`}>Oneworld</label>

                    </div>
                    <div className={classes.filter__checkbox__box}>
                        <input type="radio" name="alliance" value="ST" id="ST" className={classes.filter__checkbox__app_field} onClick={radioButtonChangeHandler}/>
                        <label htmlFor="ST" className={`${classes.filter__checkbox__label} ${filter ? classes.filter__checkbox__label_symbol : ''}`}>Sky Team</label>

                    </div>
                    <div className={classes.filter__checkbox__box}>
                        <input type="radio" name="alliance" value="SA" id="SA" className={classes.filter__checkbox__app_field} onClick={radioButtonChangeHandler}/>
                        <label htmlFor="SA" className={`${classes.filter__checkbox__label} ${filter ? classes.filter__checkbox__label_symbol : ''}`}>Star Alliance</label>

                    </div>
                </fieldset>
            </div>
            <div className={classes.boxes}>
                {dataWithAlliance?.map((data, i) => (
                    <BoxItem
                        key={i}
                        i={i}
                        name={data?.name}
                        logo={data?.logoURL}
                        link={data?.site}
                        alliance={data?.alliance}
                        phoneNumber={data?.phone}
                        isShown={isShown}
                        setIsShown={setIsShown}
                    />
                ))}
            </div>
        </main>
    );
};

export default HomePage;