import React from 'react';
import Layout from "./components/Layout/Layout";
import HomePage from "./containers/HomePage/HomePage";

const App = () => {
    return (
        <Layout>
          <HomePage/>
        </Layout>
    );
};

export default App;