import {all} from 'redux-saga/effects';
import dataSagas from "./sagas/dataSagas";

export function* rootSagas() {
    yield all([
        ...dataSagas,
    ]);
}