import {combineReducers} from "redux";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import dataSlice from "./slices/dataSlice";
import {rootSagas} from "./rootSaga";

const rootReducer = combineReducers({
    data: dataSlice.reducer,
});

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: rootReducer,
    middleware: [sagaMiddleware],
    devTools: true,
});


sagaMiddleware.run(rootSagas);

export default store;