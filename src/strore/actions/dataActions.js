import dataSlice from "../slices/dataSlice";

export const {
    fetchDataRequest,
    fetchDataSuccess,
    fetchDataFailure
} = dataSlice.actions;