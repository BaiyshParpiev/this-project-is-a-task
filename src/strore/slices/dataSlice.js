import {createSlice}  from "@reduxjs/toolkit";

const name = "data";

const dataSlice = createSlice({
    name,
    initialState: {
        data: [],
        fetchLoading: false,
        fetchError: null,
    },
    reducers: {
        fetchDataRequest(state, action){
            state.fetchLoading = true;
        },
        fetchDataSuccess(state, {payload: data}){
            state.fetchLoading = false;
            state.data = data;
            state.fetchError = null;
        },
        fetchDataFailure(state, {payload: error}){
            state.fetchLoading = false;
            state.fetchError = error;
        }
    }
});

export default dataSlice;
