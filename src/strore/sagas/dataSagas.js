import {put, takeEvery} from "redux-saga/effects";
import fetchJsonp from "fetch-jsonp";
import {
    fetchDataRequest,
    fetchDataSuccess,
    fetchDataFailure
} from '../actions/dataActions'

export function* fetchDataSaga() {
    try{
        const response = yield fetchJsonp('https://kayak.com/h/mobileapis/directory/airlines/homework', {jsonpCallback: "jsonp"})
        const data = yield response.json();
        yield put(fetchDataSuccess(
            data.filter(d => d.alliance !== 'none')
        ));
    }catch(e){
        yield put(fetchDataFailure(e));
    }
}

const dataSagas = [
    takeEvery(fetchDataRequest, fetchDataSaga),
];

export default dataSagas;