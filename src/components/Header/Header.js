import React from 'react';
import classes from './Header.module.scss';
import Logo from '../../assets/SVGs/Logo.svg'

const Header = () => {
    return (
        <header className={classes.header}>
            <div className={classes.header__content}>
                <a href="/" className={classes.header__content__logo}>
                    <img src={Logo} alt="Logo of KAYAK company"/>
                </a>
            </div>
        </header>
    );
};

export default Header;