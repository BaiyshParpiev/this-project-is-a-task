import React from 'react';
import classes from './CirccularProgress.module.scss';

const CircularProgress = () => {
    return (
        <div className={classes.loader__box}>
            <div className={classes.loader}/>
        </div>
    );
};

export default CircularProgress;