import React from 'react';
import classes from './BoxItem.module.scss';

const BoxItem = ({i, logo, name, alliance, link, phoneNumber, isShown, setIsShown}) => {

    let clearLink;
    if (link) {
        clearLink = link.replace('https://www.', '').replace('https://', '').split('/')[0];
    }


    return (
        <div className={classes.box} onMouseOver={() => setIsShown(i)} onMouseLeave={() => setIsShown(null)}>
            <div className={classes.box__inner}>
                <div className={classes.box__logo}>{<img src={"https://kayak.com" + logo} alt={`${name} logo`}/>}</div>
                <div className={classes.box__body}>
                    <h5 className={classes.box__body_title}>{name}</h5>
                    {isShown === i &&(
                        <>
                            <h6 className={classes.box__body_alliance}>{alliance}</h6>
                            <h6 className={classes.box__body_phoneNumber}>{phoneNumber}</h6>
                            <a href={link} className={classes.box__body_link}>{clearLink}</a>
                        </>
                    )}
                </div>
            </div>
        </div>
    );
};

export default BoxItem;